using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using definityfirst.DataContext;
using definityfirst.Models;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;

namespace definityfirst.webapi.integrationtest
{
    public class CustomWebApplicationFactory : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {


            builder.ConfigureServices(services =>
                {
                    
                var serviceProvider = new ServiceCollection()
                        .AddEntityFrameworkInMemoryDatabase()
                        .BuildServiceProvider();


                services.AddDbContext<MyDbContext>(options =>
                        {
                        options.UseInMemoryDatabase("InMemoryAppDb");
                        options.UseInternalServiceProvider(serviceProvider);
                    });

                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                    {
                        var scopedServices = scope.ServiceProvider;
                        var appDb = scopedServices.GetRequiredService<MyDbContext>();


                        appDb.Database.EnsureDeleted();
                        appDb.Database.EnsureCreated();

                        try
                        {

                            SeedData.PopulateTestData(appDb);
                        }
                        catch (Exception )
                        {
                           // TODO log the error
                        }
                    }
                });
        }
    }
}