using System;
using definityfirst.Models;
using definityfirst.DataContext;
using System.Collections.Generic;
using System.Linq;

namespace definityfirst.webapi.integrationtest
{
    public static class SeedData
    {
        public static void PopulateTestData(MyDbContext dbContext)
        {
            foreach (var entity in dbContext.Products) dbContext.Products.Remove(entity);
            foreach (var entity in dbContext.Categories) dbContext.Categories.Remove(entity);

            dbContext.SaveChanges();

            if (dbContext.Products.Any())
            {
                return;   // DB has been seeded
            }

             if (dbContext.Categories.Any())
            {
                return;   // DB has been seeded
            }

            // Adding Categories
            List<Category> categories = new List<Category>();

            categories.Add(new Category()
            {
                Name = "Category's Integration Test Name 1"
            });
            categories.Add(new Category()
            {
                Name = "Category's Integration Test Name 2"
            });

            dbContext.Categories.AddRange(categories);
            dbContext.SaveChanges();

            // Adding Products
            string enviroment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (!dbContext.Products.Any())
            {
                List<Product> products = new List<Product>();
                var random = new Random();
                products = Enumerable.Range(1, 100).Select(index => new Product
                {
                    Name = "Product's Integration Test Name " + enviroment + " " + index,
                    Description = "Product's  Integration Test Description " + enviroment + " " + index,
                    Price = random.Next(0, 3000),
                    Category = categories.ElementAt(random.Next(1, 2))
                }).ToList();
                dbContext.Products.AddRange(products);
                dbContext.SaveChanges();
            }

        }
    }
}