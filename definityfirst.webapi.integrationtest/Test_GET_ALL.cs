using System;
using Xunit;
using System.Net.Http;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using definityfirst.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http.Headers;

namespace definityfirst.webapi.integrationtest
{
    public class Test_GET_ALL : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly HttpClient _client;

        public Test_GET_ALL(CustomWebApplicationFactory factory)
        {
            _client = factory.CreateClient();

        }

        [Fact]
        public async Task IntegrationTest_GET_All_200OKAll()
        {
            var httpResponse = await _client.GetAsync("/api/v1/Product/");
            httpResponse.EnsureSuccessStatusCode();

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var products = JsonConvert.DeserializeObject<List<Product>>(stringResponse);
            Assert.Equal(100, products.Count);
        }

        [Fact]
        public async Task IntegrationTest_GET_All_200OKAll_Pagination()
        {
            var httpResponse = await _client.GetAsync("/api/v1/Product?page=2&pageSize=20");
            httpResponse.EnsureSuccessStatusCode();

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var products = JsonConvert.DeserializeObject<List<Product>>(stringResponse);
            Assert.Equal(20, (int)products.Count);
        }

        [Fact]
        public async Task IntegrationTest_GET_All_200OKAll_Pagination_Offset()
        {
            var httpResponse = await _client.GetAsync("/api/v1/Product?page=2&pageSize=100");
            httpResponse.EnsureSuccessStatusCode();

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var products = JsonConvert.DeserializeObject<List<Product>>(stringResponse);
            Assert.Equal(0, (int)products.Count);
        }


        [Fact]
        public async Task IntegrationTest_GET_ByID_200OK()
        {
            var httpResponse = await _client.GetAsync("/api/v1/Product/4");
            httpResponse.EnsureSuccessStatusCode();

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var product = JsonConvert.DeserializeObject<Product>(stringResponse);

            Assert.Equal(4, product.Id);
        }

        [Fact]
        public async Task IntegrationTest_GET_ByID_404NotFound()
        {
            var httpResponse = await _client.GetAsync("/api/v1/Product/4000");
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }

        [Fact]
        public async Task IntegrationTest_DELETE_404NotFound()
        {
            var httpResponse = await _client.DeleteAsync("/api/v1/Product/4000");
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }

        [Fact]
        public async Task IntegrationTest_DELETE_204NotContent()
        {
            var httpResponse = await _client.DeleteAsync("/api/v1/Product/4");
            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
        }

        [Fact]
        public async Task IntegrationTest_POST_201Created()
        {
            Product product = new Product
            {

                Name = "New Product integration test",
                Description = "New Product integration Test Descrition",
                Price = 123.00m,
                CategoryId = 1

            };
            var myContent = JsonConvert.SerializeObject(product);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var httpResponse = await _client.PostAsync("/api/v1/Product/", byteContent);
            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
        }

        [Fact]
        public async Task IntegrationTest_POST_400BadRequest()
        {
            Product product = new Product
            {

                Description = null,
                Price = 123.00m,
                CategoryId = 1

            };
            var myContent = JsonConvert.SerializeObject(product);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var httpResponse = await _client.PostAsync("/api/v1/Product/", byteContent);
            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }

        [Fact]
        public async Task IntegrationTest_PUT_204NoContent()
        {
            Product product = new Product
            {
                Id = 2,
                Name = "New Product integration test 9 Name",
                Description = "New Product integration Test 9 Descrition",
                Price = 600.00m,
                CategoryId = 1

            };
            var myContent = JsonConvert.SerializeObject(product);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var httpResponse = await _client.PutAsync("/api/v1/Product/", byteContent);
            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
        }

        [Fact]
        public async Task IntegrationTest_PUT_404NotFound()
        {
            Product product = new Product
            {
                Id = 200,
                Name = "New Product integration test 9 Name",
                Description = "New Product integration Test 9 Descrition",
                Price = 600.00m,
                CategoryId = 1

            };
            var myContent = JsonConvert.SerializeObject(product);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var httpResponse = await _client.PutAsync("/api/v1/Product/", byteContent);
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }

        [Fact]
        public async Task IntegrationTest_PUT_400BadRequest()
        {
            Product product = new Product
            {
                Id = 200,
                Description = null,
                Price = 600.00m,
                CategoryId = 1

            };
            var myContent = JsonConvert.SerializeObject(product);
            var buffer = System.Text.Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var httpResponse = await _client.PutAsync("/api/v1/Product/", byteContent);
            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }

    }
}
