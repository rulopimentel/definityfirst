using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using definityfirst.Models;
using definityfirst.DataContext;
using Microsoft.AspNetCore.Http;

namespace definityfirst.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class ProductController : ControllerBase
    {
        private MyDbContext context;

        /// <summary>
        /// ProductController Constructor
        /// </summary>
        /// <param name="context">DataService as a model of data.</param>

        public ProductController(MyDbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get product by Id
        /// </summary>
        /// <param name="Id">Product Id to get</param>
        /// <returns>
        /// 404 Not Found the Product Id
        /// 200 Ok Found the Product and return it
        /// </returns>
        /// <response code="404">Not Found the Product Id</response>
        /// <response code="200">Return the Product</response>            

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Get(long Id)
        {
            Product product = context.Products.Find(Id);
            if (product == null) return NotFound(null);
            else return Ok(product);
        }

        /// <summary>
        /// Get all products
        /// </summary>
        /// <param name="page">A nullable int to define the page number to return.</param>
        /// <param name="pageSize">A optional int to define the page size. The default value is 20</param>
        /// <returns>
        /// 200 Ok All products or paginated results
        /// </returns>
        /// <response code="200">All products or paginated results</response> 
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IEnumerable<Product> Get(int? page = null, int pageSize = 20)
        {
            if (!page.HasValue) return context.Products;
            else
            {
                return context.Products
                 .Skip((page.Value - 1) * pageSize)
                 .Take(pageSize)
                 .ToList();
            }
        }

        /// <summary>
        /// Delete an specified product
        /// </summary>
        /// <param name="Id">Product Id to be delete</param>
        /// <returns>
        /// 204 Not Content - When the product does exist and was delete.
        /// 404 Not Found - When the product doesn't exist.
        /// </returns>
        /// <response code="204">When the product does exist and was delete.</response>
        /// <response code="404">When the product doesn't exist.</response>   
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult Delete(long Id)
        {
            Product product = context.Products.Find(Id);
            if (product == null) return NotFound(null);
            else
            {
                context.Products.Remove(product);
                context.SaveChanges();
                return NoContent();
            }
        }

        /// <summary>
        /// Create an new product
        /// </summary>
        /// <param name="productViewModel">Product object to be created</param>
        /// <returns>
        /// 400 Bad Request - When a parameter is empty.
        /// 201 Created - Also, return a body with the created Product and the Location Header
        /// </returns>
        /// <response code="400"> When Model is invalid.</response>
        /// <response code="201">Return a body with the created Product and the Location Header</response>   
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public IActionResult Post(ProductViewModel productViewModel)
        {

            if (productViewModel == null || !ModelState.IsValid) return BadRequest(null);

            Category category = context.Categories.Find(productViewModel.CategoryId);
            Product product = new Product()
            {
                Name = productViewModel.Name,
                Description = productViewModel.Description,
                Price = productViewModel.Price,
                Category = category == null ? null : category
            };

            context.Products.Add(product);
            context.SaveChanges();
            return Created(new Uri($"{product.Id}", UriKind.Relative), product);

        }

        /// <summary>
        /// Update an new product
        /// </summary>
        /// <param name="productViewModel">Product object to be updated</param>
        /// <returns>
        /// 400 Bad Request - When a parameter is empty, o when a CategoryId doesn't exists.
        /// 404 Not Found - When the product doesn't exist.
        /// 204 Not Content - When the Product was updated.
        /// </returns>
        /// <response code="400"> When Model is invalid.</response>
        /// <response code="404">When the product doesn't exist.</response>   
        /// <response code="204">When the Product was updated.</response>   

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Put(ProductViewModel productViewModel)
        {
            if (!ModelState.IsValid) return BadRequest(null);

            Product product = context.Products.Find(productViewModel.Id);
            if (product == null) return NotFound(null);

            Category category = context.Categories.Find(productViewModel.CategoryId);
            product.Name = productViewModel.Name;
            product.Description = productViewModel.Description;
            product.Price = productViewModel.Price;
            product.Category = category == null ? null : category;
            context.SaveChanges();

            return NoContent();
        }


    }



}

