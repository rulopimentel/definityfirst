using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace definityfirst.Models
{
    /// <summary>
    /// Product Class
    /// </summary>
    /// <remarks>
    /// This class contains Id, Name, Description, Price and a List of Category.
    /// </remarks>

    public class Product
    {
        // Product's Id
        [Key]
        public long Id { get; set; }

        // Product's Name
        [Required]
        public string Name { get; set; }

        // Product's Description
        [Required]
        public string Description { get; set; }

        // Product's Price
        [Required]
        public decimal Price { get; set; }
        
        public long CategoryId { get; set; }
        // Product's Category
        public virtual Category Category { get; set; }

    }

    public class ProductViewModel
    {
        public long Id { get; set; }
        // Product's Name
        [Required]
        public string Name { get; set; }

        // Product's Description
        [Required]
        public string Description { get; set; }

        // Product's Price
        [Required]
        public decimal Price { get; set; }

        // Product's Category
        [Required]
        public long CategoryId { get; set; }

    }


}