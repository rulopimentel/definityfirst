
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace definityfirst.Models
{
    /// <summary>
    /// Category Class
    /// </summary>
    /// <remarks>
    /// This class was created to specify a cardinality 0 to many between Product and Category. It contains Id, Name
    /// </remarks>

    public class Category
    {
        // Category's ID
        [Key]
        public long Id { get; set; }

        // Category's Name
        [Required]
        public string Name { get; set; }


    }




}