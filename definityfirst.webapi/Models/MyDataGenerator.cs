using System;
using definityfirst.Models;
using definityfirst.DataContext;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;

public class MyDataGenerator
{

    /// <summary>
    /// Initialize data
    /// </summary>
    public static void Initialize(IServiceProvider serviceProvider)
    {
        using (var context = new MyDbContext(
            serviceProvider.GetRequiredService<DbContextOptions<MyDbContext>>()))
        {

            // Adding Categories
            List<Category> categories = new List<Category>();
            if (!context.Categories.Any())
            {

                categories.Add(new Category()
                {
                    Id = 1,
                    Name = "Category's Name 1"
                });
                categories.Add(new Category()
                {
                    Id = 2,
                    Name = "Category's Name 2"
                });
                categories.Add(new Category()
                {
                    Id = 3,
                    Name = "Category's Name 3"
                });
                context.Categories.AddRange(categories);
                context.SaveChanges();
            }

            // Adding Products
            string enviroment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            if (!context.Products.Any())
            {
                List<Product> products = new List<Product>();
                var random = new Random();
                products = Enumerable.Range(1, 1000).Select(index => new Product
                {
                    Id = index,
                    Name = "Product's Name " + enviroment + " " + index,
                    Description = "Product's Description " + enviroment + " " + index,
                    Price = random.Next(0, 3000),
                    Category = categories.ElementAt(random.Next(1, 3))
                    //CategoryId = 2
                })
                .ToList();
                context.Products.AddRange(products);
                context.SaveChanges();
            }





        }
    }
}