FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

# COPY csporj and restore the projects
COPY definityfirst.sln ./definityfirst.sln
COPY definityfirst.webapi/definityfirst.csproj ./definityfirst.webapi/definityfirst.csproj
COPY definityfirst.webapi.test/definityfirst.webapi.test.csproj ./definityfirst.webapi.test/definityfirst.webapi.test.csproj
COPY definityfirst.webapi.integrationtest/definityfirst.webapi.integrationtest.csproj ./definityfirst.webapi.integrationtest/definityfirst.webapi.integrationtest.csproj
RUN dotnet restore

# COPY all and build
COPY . ./
RUN dotnet test definityfirst.webapi.test -c Release
RUN dotnet test definityfirst.webapi.integrationtest -c Release
RUN dotnet publish definityfirst.webapi -c Release -o /app/out

#build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build-env /app/out ./
ENTRYPOINT ["dotnet", "definityfirst.dll"]
