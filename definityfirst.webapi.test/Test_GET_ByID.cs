using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using definityfirst.Controllers;
using definityfirst.DataContext;
using definityfirst.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace defintyfirst.webapi.test
{
    [TestClass]
    public class Test_GET_ByID
    {


        [TestMethod]
        [Timeout(2000)]  // 2 seconds or abort
        public void Test_GET_ByID_200OK()
        {
            var dbContext = DbContextMocker.GetMyDbContext(nameof(Test_GET_ByID_200OK));
            ProductController productController = new ProductController(dbContext);
            IActionResult actionResult = productController.Get(4);

            Product product = dbContext.Products.Find(4L);
            dbContext.Dispose();

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(OkObjectResult));
            Assert.IsInstanceOfType((actionResult as OkObjectResult).Value, typeof(Product));
            Assert.AreSame((actionResult as OkObjectResult).Value as Product, product);
        }

        [TestMethod]
        [Timeout(2000)]  // 2 seconds or abort
        public void Test_GET_ByID_404NotFound()
        {
            var dbContext = DbContextMocker.GetMyDbContext(nameof(Test_GET_ByID_404NotFound));
            ProductController productController = new ProductController(dbContext);
            IActionResult actionResult = productController.Get(4000);

            Product product = dbContext.Products.Find(4L);
            dbContext.Dispose();

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundObjectResult));
        }

    }
}
