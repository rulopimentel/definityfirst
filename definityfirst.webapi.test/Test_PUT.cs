using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using definityfirst.Controllers;
using definityfirst.DataContext;
using definityfirst.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace defintyfirst.webapi.test
{
    [TestClass]
    public class Test_PUT
    {

        [TestMethod]
        [Timeout(2000)]  // 2 seconds or abort
        public void Test_PUT_204NoContent()
        {
            var dbContext = DbContextMocker.GetMyDbContext(nameof(Test_PUT_204NoContent));
            ProductController productController = new ProductController(dbContext);

            ProductViewModel product = new ProductViewModel
            {
                Id = 9,
                Name = "New Product test 9 Name",
                Description = "New Product Test 9 Descrition",
                Price = 600.00m,
                CategoryId = 3
            };

            IActionResult actionResult = productController.Put(product);
            dbContext.Dispose();

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NoContentResult));
        }

        [TestMethod]
        [Timeout(2000)]  // 2 seconds or abort
        public void Test_PUT_404NotFound()
        {
            var dbContext = DbContextMocker.GetMyDbContext(nameof(Test_PUT_404NotFound));
            ProductController productController = new ProductController(dbContext);

            ProductViewModel product = new ProductViewModel
            {
                Id = 9000,
                Name = "New Product test 9 Name",
                Description = "New Product Test 9 Descrition",
                Price = 600.00m,
                CategoryId = 3
            };

            IActionResult actionResult = productController.Put(product);
            dbContext.Dispose();

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundObjectResult));
        }

        //TODO Make another validation for 400 Status Code

    }
}
