using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using definityfirst.Controllers;
using definityfirst.DataContext;
using definityfirst.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace defintyfirst.webapi.test
{
    [TestClass]
    public class Test_GET_All
    {


        [TestMethod]
        [Timeout(2000)]  // 2 seconds or abort
        public void Test_GET_All_200OKAll()
        {
            var dbContext = DbContextMocker.GetMyDbContext(nameof(Test_GET_All_200OKAll));
            ProductController productController = new ProductController(dbContext);
            IEnumerable<Product> result = productController.Get();
            int size = result.Count();
            dbContext.Dispose();

            // Assert
            Assert.IsInstanceOfType(result, typeof(IEnumerable<Product>));
            Assert.AreEqual(100, size);
        }

        [TestMethod]
        [Timeout(2000)]  // 2 seconds or abort
        public void Test_GET_All_200OKAll_Pagination()
        {
            var dbContext = DbContextMocker.GetMyDbContext(nameof(Test_GET_All_200OKAll_Pagination));
            ProductController productController = new ProductController(dbContext);
            IEnumerable<Product> result = productController.Get(2, 20);
            int size = result.Count();

            // Assert
            Assert.IsInstanceOfType(result, typeof(IEnumerable<Product>));
            Assert.AreEqual(20, size);
            dbContext.Dispose();

        }

        [TestMethod]
        [Timeout(2000)]  // 2 seconds or abort
        public void Test_GET_All_200OKAll_Pagination_Offset()
        {
            var dbContext = DbContextMocker.GetMyDbContext(nameof(Test_GET_All_200OKAll_Pagination_Offset));
            ProductController productController = new ProductController(dbContext);
            IEnumerable<Product> result = productController.Get(2, 100);
            int size = result.Count();
            dbContext.Dispose();

            // Assert
            Assert.IsInstanceOfType(result, typeof(IEnumerable<Product>));
            Assert.AreEqual(0, size);
        }





    }
}
