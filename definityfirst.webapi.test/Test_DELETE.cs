using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using definityfirst.Controllers;
using definityfirst.DataContext;
using definityfirst.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace defintyfirst.webapi.test
{
    [TestClass]
    public class Test_DELETE
    {


        [TestMethod]
        [Timeout(2000)]  // 2 seconds or abort
        public void Test_DELETE_404NotFound()
        {
            var dbContext = DbContextMocker.GetMyDbContext(nameof(Test_DELETE_404NotFound));
            ProductController productController = new ProductController(dbContext);
            IActionResult actionResult = productController.Delete(4000);

            dbContext.Dispose();

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundObjectResult));
        }

        [TestMethod]
        [Timeout(2000)]  // 2 seconds or abort
        public void Test_DELETE_204NotContent()
        {
            var dbContext = DbContextMocker.GetMyDbContext(nameof(Test_DELETE_204NotContent));
            ProductController productController = new ProductController(dbContext);
            IActionResult actionResult = productController.Delete(40);

            dbContext.Dispose();

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NoContentResult));
        }

    }
}
