using System;
using definityfirst.Models;
using definityfirst.DataContext;
using System.Collections.Generic;
using System.Linq;

namespace defintyfirst.webapi.test
{
    public static class MyDbContextExtensions
    {
        public static void Seed(this MyDbContext context)
        {
            // Adding Categories
            List<Category> categories = new List<Category>();

            categories.Add(new Category()
            {
                Id = 1,
                Name = "Category's Test Name 1"
            });
            categories.Add(new Category()
            {
                Id = 2,
                Name = "Category's Test Name 2"
            });
           
            context.Categories.AddRange(categories);
            context.SaveChanges();


            // Adding Products
            string enviroment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            List<Product> products = new List<Product>();
            var random = new Random();
            products = Enumerable.Range(1, 100).Select(index => new Product
            {
                Id = index,
                Name = "Product's Test Name " + enviroment + " " + index,
                Description = "Product's Test Description " + enviroment + " " + index,
                Price = random.Next(0, 3000),
                Category = categories.ElementAt(random.Next(1, 2))
            }).ToList();
            context.Products.AddRange(products);
            context.SaveChanges();
        }
    }
}