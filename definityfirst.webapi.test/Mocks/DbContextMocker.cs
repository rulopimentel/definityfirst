using Microsoft.EntityFrameworkCore;
using definityfirst.Models;
using definityfirst.DataContext;


namespace defintyfirst.webapi.test
{
    public static class DbContextMocker
    {
        public static MyDbContext GetMyDbContext(string dbName)
        {
            // Create options for DbContext instance
            var options = new DbContextOptionsBuilder<MyDbContext>()
                .UseInMemoryDatabase(databaseName: dbName)
                .Options;

            // Create instance of DbContext
            var dbContext = new MyDbContext(options);

            // Add entities in memory
            dbContext.Seed();

            return dbContext;
        }
    }
}