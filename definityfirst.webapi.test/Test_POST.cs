using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using definityfirst.Controllers;
using definityfirst.DataContext;
using definityfirst.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace defintyfirst.webapi.test
{
    [TestClass]
    public class Test_POST
    {

        [TestMethod]
        [Timeout(2000)]  // 2 seconds or abort
        public void Test_POST_201Created()
        {
            var dbContext = DbContextMocker.GetMyDbContext(nameof(Test_POST_201Created));
            ProductController productController = new ProductController(dbContext);

            ProductViewModel product = new ProductViewModel
            {
                Name = "New Product test",
                Description = "New Product Test Descrition",
                Price = 123.00m,
                CategoryId = 1
            };

            IActionResult actionResult = productController.Post(product);
            dbContext.Dispose();

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(CreatedResult));
        }

        //TODO Make another validation for 400 Status Code

    }
}
