# Requirements

## R0001 - Create a Product 👍🏻
## R0002 - Update a Product 👍🏻
## R0003 - Delete a Product 👍🏻
## R0004 - Get a Product 👍🏻
## R0005 - Get All a Product 👍🏻
## R0006 - Docker image 👍🏻 
        $ docker build -t definityfirst.webapi -f Dockerfile .
        $ docker run -d -p 8080:80 --name definityfirst.webapi definityfirst.webapi
## R0007 - Document the Web API using swagger 👍🏻
## R0008 - Create Unit and Integration tests 👍🏻
## R0009 - Versioning 👍🏻
## R0010 - Submit Git to a remote: https://bitbucket.org/rulopimentel/definityfirst/ or clone git clone https://rulopimentel@bitbucket.org/rulopimentel/definityfirst.git 👍🏻




